/*
* charlie_plexing.c
*
* Created: 14-8-2020 13:00:14
* Author : wilko
*
* ATTINY10 (6 pins) at 1 MHz
*
* electronic dice with 6 leds and 1 button
* when pressed the leds rotate through numbers 1 to 6
* when the button is released the rotation slows down
* to a halt
* to conserve power the dice shuts itself down after
* approx 10 seconds no activity of the button
*
* on 3.3V the device uses about 1mA when active
* and 1uA in shutdown mode
*/

#define F_CPU 1000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

void slowdown(uint8_t ms);

int main(void)
{
	uint8_t number = 1;
	uint8_t speed = 4;
	uint16_t timeout = 1000;

	CCP = 0xD8;									//enable configuration change
	CLKMSR = 0x00;								//8 MHz oscillator
	CCP = 0xD8;									//enable configuration change
	CLKPSR = 0x03;								//prescaler = 8 (1 MHz)

	PRR = 0x03;									//power reduction ADC & TIM0

	DDRB = 0x00;								//all ports are inputs

	sei();										//enable global interrupts

	while (1)
	{
		timeout--;								//count down from 1000 to 0

		DDRB = 0x00;							//all inputs
		_delay_ms(2);							//needed because of capacity of leds?
		if ((PINB & 0x01) == 0)					//is PB0 low? then button is pressed
		{
			speed = 4;							//rotation fast
			timeout = 1000;						//restart timeout counter
		}

		if (speed < 100)						//stop rotating at 100ms steps
		{
			speed += 2;							//slow down rotation
			if (number < 6 ) number++;			//cycle between number 1 through 6
			else number = 1;
			slowdown(speed);
		}

		switch (number)							//set in/outputs needed for charlieplexing
		{
		case 6:
			DDRB = 0x05;						//led6
			PORTB = 0x01;
			break;

		case 5:
			DDRB = 0x05;						//led5
			PORTB = 0x04;
			break;

		case 4:
			DDRB = 0x6;							//led4
			PORTB = 0x02;
			break;

		case 3:
			DDRB = 0x6;							//led3
			PORTB = 0x04;
			break;

		case 2:
			DDRB = 0x3;							//led2
			PORTB = 0x01;
			break;

		case 1:
			DDRB = 0x3;							//led1
			PORTB = 0x02;
			break;

		default:
			DDRB = 0x00;						//all off
			PORTB = 0x00;
			break;
		}
		_delay_ms(8);

		if (timeout == 0)						//after 10 seconds of button-inactivity
		{
			DDRB = 0x00;						//all ports as inputs
			PUEB = 0x0F;						//with (weak) pullup resistors
			PCMSK = 0x01;						//PCINT0 enable  (pin change interrupt)
			PCICR = 0x01;						//enable pin change interrupts
			PCIFR = 0x01;						//clear pin change interrupt flag

			set_sleep_mode(SLEEP_MODE_PWR_DOWN);//shutdown mcu, will only wakeup
			sleep_mode();						//from external generated interrupt

			//sleeping until pin change interrupt
			/************************************************************************/
			//wake up because of PCINT0 interrupt

			PUEB = 0x00;						//disable all pullups
			PCMSK = 0x00;						//PCINT0 disable  (pin change interrupt)
			PCICR = 0x01;						//disable pin change interrupts
			PCIFR = 0x01;						//clear pin change interrupt flag
			timeout = 1000;						//10 seconds
		}
	}
}

void slowdown(uint8_t ms)
{
	volatile uint8_t millisec;

	millisec = ms;
	while (millisec--)
	{
		_delay_ms(2);
	}
}


ISR(PCINT0_vect)
{
	//nothing to do here
	//we only needed to wakeup
}