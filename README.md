# CharliePlexing on a ATTINY10

CharliePlexing with 6 leds and 1 button. When pressed the leds rotate through numbers 1 to 6.
After the button is released the rotation slows down to a halt.
To conserve power the device shuts itself down after approx 10 seconds.

On 3.3V the device uses about 1mA when active and 1uA in shutdown mode.